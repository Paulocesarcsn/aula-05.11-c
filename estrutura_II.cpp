//Calcular a �rea total de uma casa com tr�s comodos retangulares
//Seu programa deve ler as dimens�es de cada comodo,
// calcular e mostrar a �rea deste e, ao final, mostrar
// a area total da casa
#include	<stdio.h>

struct	comodo {
	int			lado1;
	int			lado2;
	int			area;
};

struct	casa {
	struct comodo	CMDS[3];
	int				areaTOTAL;
};

int	main()
{
	// declara as vari�veis
	struct casa	CASA;
	int			i;
	
	for (i = 0; i < 3; i++)
	{
		printf("Digite os lados de um comodo\n");
		
		scanf("%d%d", &CASA.CMDS[i].lado1, &CASA.CMDS[i].lado2);
		CASA.CMDS[i].area = CASA.CMDS[i].lado1 * CASA.CMDS[i].lado2;
		
		printf("Comodo %d: %d por %d e area %d\n", i+1,
		CASA.CMDS[i].lado1, CASA.CMDS[i].lado2, CASA.CMDS[i].area);
	}
}// paulocezarcsn@gmail.com
