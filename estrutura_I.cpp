//Criar um programa que declare uma estrutura �pessoa" contendo
//nome (m�ximo de 100 char), idade e endere�o, sendo endere�o
//outra estrutura definida previamente contendo: rua, numero e cep;
//Esse programa deve ler os dados do usu�rio e imprimi-los
//na tela na sequencia;
#include	<stdio.h>

struct	end {
	char		rua[256];
	int			numero;
	int			CEP;
};

struct	pess {
	char		nome[100];
	int			idade;
	struct end	endereco;
};

int	main()
{
	// declara as vari�veis
	struct pess	Pessoa;
	
	// leitura dos dados
	printf("Digite o nome:");
	scanf("%[^\n]", &Pessoa.nome);
	
	printf("\nDigite idade:");
	scanf("%d", &Pessoa.idade);
	
	fflush(stdin);
	
	printf("\nDigite o endereco:");
	printf("\nDigite rua:");
	
	scanf("%[^\n]", &Pessoa.endereco.rua);
	
	printf("\nDigite numero:");
	scanf("%d", &Pessoa.endereco.numero);
	
	fflush(stdin);
	
	printf("\nDigite CEP:");
	scanf("%d", &Pessoa.endereco.CEP);
	
	printf("Nome: %s\nIdade: %d\nRua: %s\n Numero: %d\nCEP: %d\n",
	Pessoa.nome, Pessoa.idade, Pessoa.endereco.rua,
	Pessoa.endereco.numero, Pessoa.endereco.CEP);
	
}
